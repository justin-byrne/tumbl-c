#!/bin/bash
#
# tumbdl-c - fork of tumbdl.sh: a tumblr image downloader
#
# Usage: tumbdl-c [URL] [DIR]
#
# URL: URL of tumblelog
# DIR: directory to put images in
#
# Example: . tumbdl-c user.tumblr.com <dir>
# or       ./tumbdl-c user.tumblr.com <dir>
# or  source tumbdl-c user.tumblr.com <dir>
#
# Requirements: curl, PCRE for grep -P (normally you have this)
#
# Should also work for tumblelogs that have their own domain.
#
# If you use and like this script, please donate bitcoin to the original author
# my address: 1LsCBob5B9SWknoZfF6xpZWJ9GF4NuBLVD
##
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# include: tputs.clr
# inc: bash shell function library & tputs colors

# Get: current bash environment
# Feel free to change the following settings in relation to your system configuration(s)
os=$(uname -s)

case "$os" in
               Linux) . tputs.clr                                             # [LINUX]
                      echo "${fg_cyan}[tputs]${reset} loaded! for Linux!"
                      ;;
  MINGW32_NT-6.1-WOW) . tputs.clr                                             # [MINGW32]
                      echo "${fg_cyan}[tputs]${reset} loaded! for MINGW32!"
                      ;;
esac

url=$1
targetDir=$2

# global curl options
# to disable progress bar, replace with -s
# to enable verbose mode, add -v
curlOptions='--progress-bar'
userAgent='Mozilla/5.0 (Windows NT 6.1; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0'

# check usage
if [ $# -ne 2 ]; then
  echo "Usage: tumbdl [URL] [DIR]"
  echo ""
  echo "URL: URL of tumblelog, e.g. prostbote.tumblr.com"
  echo "DIR: directory to put images in, e.g. prostbote"
  exit
fi

# sanitize input url
url=$(echo "$url" | sed 's/http[s]*:\/\///g;s/\/$//g')

# create target dir
mkdir "$targetDir"
touch "$targetDir/articles.txt"

# create cookie jar (not really needed atm)
cookieFile="$(mktemp 2>/dev/null || mktemp -t 'mytmpdir')"

# get first archive page
archiveLink="/archive/"

# loop over archive pages
endOfArchive=0
while [[ $endOfArchive -ne 1 ]]
do
  # get archive page
  archivePage=$(curl $curlOptions -c $cookieFile --referer "http://$url" -A "$userAgent" "$url$archiveLink")
  echo -e "${fg_green}Retrieving archive page${fg_magenta} $url$archiveLink${reset}..."

  # extract links to posts
  monthPosts=$(echo "$archivePage" | grep -o -P "/post/[0-9]*.*?\"" | sed 's/"//g')

  # process all posts on this archive page
  for postURL in $(echo "$monthPosts")
  do
    # check if post page has already been processed before
    if grep -Fxq "$postURL" "$targetDir/articles.txt"
    then
      echo -e "${bg_red}${fg_black}Already got $url$postURL, skipping.${reset}"
    else
      # get the image links (can be multiple images in sets)
      echo -e "${fg_green}Retrieving post${fg_yellow} $url$postURL${reset}..."
      postPage=$(curl $curlOptions -b $cookieFile --referer "http://$url$archiveLink" -A "$userAgent" "$url$postURL")
      imageLinks=$(echo "$postPage" | grep -o -P "http[s]*://([0-9]*.)?media\.tumblr\.com/([A-Za-z0-9]*/)?tumblr_[A-Za-z0-9]*_[0-9]*\.[a-z]*" | sort | uniq)
      # remove resolution info from image filename
      baseImages=$(echo "$imageLinks" | grep -o "tumblr_.*$" | sed 's/_[0-9]*\.\w*//g' | uniq)
      # if we encounter any download errors, don't mark the post as archived
      curlError=0

      # determine the highest available resolution and download image
      if [ ! -z "$baseImages" ]
      then

        for image in $(echo "$baseImages")
        do
          # get the image name of image with highest resolution
          maxResImage=$(echo "$imageLinks" | grep -o "$image.*" | sort -n | head -n 1)
          # get full image url
          maxResImageURL=$(echo "$imageLinks" | grep "$maxResImage")
          # download image (if it doesn't exist)
          if [ -e "$targetDir/$maxResImage" ]
          then
            echo -e "${fg_red}Image exists, skipping.${reset}"
          else
            echo -e "${fg_cyan}Downloading image${fg_yellow} $maxResImageURL...${reset}"
            curl $curlOptions -b $cookieFile --referer "http://$url$postURL" -A "$userAgent" -o "$targetDir/$maxResImage" "$maxResImageURL"
            if [ ! 0 -eq $? ]; then curlError=1; fi;
          fi
        done
      else
        # no images found, check for video links
        echo -e "${fg_magenta}No images found, checking for videos${reset}"

        # check for tumblr hosted videos
        videoPlayers=$(echo "$postPage" | grep -o -P "http[s]*://www.tumblr.com/video/.*/[0-9]*/[0-9]*/" | sort | uniq)
        for video in $(echo "$videoPlayers")
        do
          echo -e "${fg_green}Found tumblr-hosted video${reset} $video"
          # get video link and type
          videoSource=$(curl $curlOptions -b $cookieFile --referer "http://$url$postURL" -A "$userAgent" "$video" | grep -o -P "<source src=\"http[s]*://www.tumblr.com/video_file/.*?>")
          # get video url
          videoURL=$(echo "$videoSource" | grep -o -P "http[s]*://www.tumblr.com/video_file/[[:0-9A-Za-z]*/]*[0-9]*/tumblr_[A-Za-z0-9]*")
          # construct filename with extension from type string
          videoFile=$(echo "$videoSource" | grep -o -P "tumblr_.*?>" | sed -e 's/<source src=\"//g' -e 's/\" type=\"video\//./g' -e 's/\">//g' -e 's/\//\_/g')
          # download video (if it doesn't exist)
          if [ -e "$targetDir/$videoFile" ]
          then
            echo -e "${fg_red}Video exists, skipping.${reset}"
          else
            echo "${fg_green}Downloading video${reset} $videoURL"
            curl $curlOptions -L -b $cookieFile --referer "http://$url$postURL" -A "$userAgent" -o "$targetDir/$videoFile" "$videoURL"
            if [ ! 0 -eq $? ]; then curlError=1; fi;
          fi
        done
        # check if youtube-dl is available
        if hash youtube-dl 2>/dev/null
        then
          # gather embedded video urls
          otherSource=""
          # check for instagram video
          otherSource=$(echo "$otherSource"; echo "$postPage" | grep -o -P "http[s]*://www.instagram.com/p/[A-Za-z0-9]*")
          # check fou youtube video
          otherSource=$(echo "$otherSource"; echo "$postPage" | grep -o -P "http[s]*://www.youtube.com/embed/.*?\?" | sed 's/\?//g')
          # check for vine
          otherSource=$(echo "$otherSource"; echo "$postPage" | grep -o -P "http[s]*://vine.co/v/.*?/")
          # check for vimeo
          otherSource=$(echo "$otherSource"; echo "$postPage" | grep -o -P "http[s]*://player.vimeo.com/video/[0-9]*")
          # check for dailymotion
          otherSource=$(echo "$otherSource"; echo "$postPage" | grep -o -P "http[s]*://www.dailymotion.com/embed/video/[A-Za-z0-9]*")
          # check for brightcove
          otherSource=$(echo "$otherSource"; echo "$postPage" | grep -o -P "http[s]*://players.brightcove.net/.*/index.html\?videoId=[0-9]*")
          # add expressions for other video sites here like this:
          #otherSource=$(echo "$otherSource"; echo "$postPage" | grep -o "http[s]*://www.example.com/embed/video/[A-Za-z0-9]*")

          # if video links were found, try youtube-dl
          if [ ! -z $otherSource ]
          then
            for otherVid in $(echo "$otherSource")
            do
              echo "${fg_magenta}Found embedded video${fg_cyan} $otherVid, ${fg_magenta}attempting download via youtube-dl...${reset}"
              youtube-dl "$otherVid" -o "$targetDir/%(title)s_%(duration)s.%(ext)s" -ciw
              # if error occurs, don't mark post as archived
              if [ ! 0 -eq $? ]; then curlError=1; fi;
            done
          else
            echo -e "${fg_red}No videos found, moving on.${reset}"
          fi
        else
          echo -e "${fg_red}youtube-dl not installed, not checking for externally hosted videos.${reset}"
        fi
      fi

      # if no error occured, enter page as downloaded
      if [[ $curlError -eq 0 ]]
      then
        echo "$postURL" >> "$targetDir/articles.txt"
      else
        echo -e "${fg_red}Some error occured during downloading. No articles.txt entry created.${reset}"
      fi

    fi
  done
  # get link to next archive page
  archiveLink=$(echo "$archivePage" | grep -o -P "id=\"next_page_link\" href=\".*?\"" | sed -e 's/id=\"next_page_link\" href=\"//g' -e 's/\"//g')
  # check if we are at the end of the archive (no link is returned)
  if [ -z "$archiveLink" ]
  then
    endOfArchive=1
    echo -e "${bg_cyan}${fg_black}Reached the last archive page. Done!${reset}"
  else
    echo -e "${bg_yellow}${fg_blue}Next archive page:${reset} $url$archiveLink"
  fi
done

echo -e "${fg_white}${bg_magenta}Complete!${reset}"
